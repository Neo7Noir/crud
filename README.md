# JWS

## URL

`localhost:3000`

## Installation

```shell
npm install
docker-compose up
```
## Methods

### Login
POST
localhost:3000/auth/login
example:

```shell
{
    "username": "admin",
    "password": "admin"
}
```


### Register
POST
localhost:3000/auth/register
example:
```shell
{
    "username": "user",
    "password": "user",
    "role": "user"
}
```

### Get all users
GET
localhost:3000/users

### Get one user
GET
localhost:3000/users/:id

### Create user
POST
localhost:3000/users

```shell
{
    "username": "",
    "password": "",
    "role": ""
}
```


### Delete user
DELETE
localhost:3000/users/:id


### Update
PUT
localhost:3000/users/:id
```shell
{
    "username": "",
    "password": ""
}
```