FROM node:18.11.0

WORKDIR /usr/src/app

COPY ["package.json", "package-lock.json", "tsconfig.json", ".env", "nodemon.json", "./"]

COPY ./src ./src

RUN npm install

CMD npm run dev
