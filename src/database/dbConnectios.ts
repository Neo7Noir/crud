import { DataSource } from "typeorm";
import { User } from "./entities/user";

export const AppDataSource = new DataSource({
    type : "postgres",
    host : "host.docker.internal",
    port : 5431,
    url : process.env.DATABASE_URL,
    entities : [User],
    synchronize : true,
})