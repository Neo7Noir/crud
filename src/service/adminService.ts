import { hash } from "bcrypt"
import { AppDataSource } from "../database/dbConnectios"
import { User } from "../database/entities/user"
import { userDto } from "../controllers/dto/userDto"

async function getAllUsers() : Promise<User[]>{
    const userRepository = AppDataSource.getRepository(User)

    const users = await userRepository.find()

    return users
}

async function getUserById(id: string) : Promise<User>{
    const userRepository = AppDataSource.getRepository(User)

    const user = await userRepository.findOne({
        where: {
            id: id
        }
    })

    return user
}

async function deleteById(id: string) {
    const userRepository = AppDataSource.getRepository(User)

    await userRepository.delete({
        id: id
    })
}

async function updateById(id: string, updUser: userDto) {
    const userRepository = AppDataSource.getRepository(User)

    updUser.password = await hash(updUser.password, 7)

    await userRepository.update({
        id: id
    }, {
        ...updUser
    })
}

async function checkUser(username : string) : Promise<User>{
    const userRepository = AppDataSource.getRepository(User)

    const user = await userRepository.findOne({
        where : {
            username : username
        }
    })

    return user
}

async function createNewUser(username : string, password : string, role : string) : Promise<User>{
    try {
        const user = await checkUser(username)

        if(user){
            throw new Error("Username already exist!")
        }

        const hashPassword = await hash(password, 7)

        const newUser = User.create({
            username : username,
            password : hashPassword,
            role : role
        })

        await newUser.save()

        return newUser
    } catch (error) {
        throw new Error(error.message)
    }
}

async function createDefaultUsers() {
    let check = await checkUser("admin")

    if(!check){
        let hashPassword = await hash("admin", 7)

        const adminUser = User.create({
            username : "admin",
            password : hashPassword,
            role : "admin"
        })
    
        await adminUser.save()
    
        hashPassword = await hash("user", 7)
        const userUser = User.create({
            username : "user",
            password : hashPassword,
            role : "user"
        })
    
        await userUser.save()
    }
}

export{
    getAllUsers,
    getUserById,
    deleteById,
    updateById,
    createNewUser,
    createDefaultUsers
}