import { compareSync, hash } from "bcrypt"
import { sign } from "jsonwebtoken"
import { AppDataSource } from "../database/dbConnectios"
import { User } from "../database/entities/user"

function createToken(userId : string, username : string, role : string) : Promise<string>{
    return new Promise<string>((resolve, reject) => {
        sign({ userId, username, role }, process.env.JWT_SECRET_KEY, {
            expiresIn : '24h'
        }, (err, token) => {
            if(err){
                reject(err)
            }
            resolve(token)
        })
    })
}

async function checkUser(username : string) : Promise<User>{
    const userRepository = AppDataSource.getRepository(User)

    const user = await userRepository.findOne({
        where : {
            username : username
        }
    })

    return user
}

async function createUser(username : string, password : string, role : string) : Promise<User>{
    try {
        const user = await checkUser(username)

        if(user){
            throw new Error("Username already exist!")
        }

        const hashPassword = await hash(password, 7)

        const newUser = User.create({
            username : username,
            password : hashPassword,
            role : role
        })

        await newUser.save()

        return newUser
    } catch (error) {
        throw new Error(error.message)
    }
}

async function loginUser(username : string, password : string) : Promise<User>{
    try {
        const user = await checkUser(username)

        if(!user){
            throw new Error('Invalid account!')
        }

        let comparePassword = compareSync(password, user.password)

        if(!comparePassword){
            throw new Error('Incorect password!')
        }

        return user
    } catch (error) {
        throw new Error(error.message) 
    }
}

export {
    createUser,
    createToken,
    loginUser,
}