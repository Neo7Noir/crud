import { Router } from "express"
import { checkRole } from "../middleware/authMiddleware"
import { addUser, deleteUser, getUser, getUsers, updateUser } from "../controllers/adminController"

const usersRoutes = Router()

usersRoutes.get('/', getUsers)
usersRoutes.post('/', checkRole(['admin']), addUser)
usersRoutes.get('/:id', checkRole(['admin']), getUser)
usersRoutes.delete('/:id', checkRole(['admin']), deleteUser)
usersRoutes.put('/:id', checkRole(['admin']), updateUser)

export { usersRoutes } 