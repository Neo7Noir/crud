import { Router } from "express";
import { authRoutes } from "./authRouter"
import { usersRoutes } from "./usersRouter";

const routes = Router()

routes.use('/auth', authRoutes)
routes.use('/users', usersRoutes)

export { routes }