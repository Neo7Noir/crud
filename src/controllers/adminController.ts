import { createNewUser, deleteById, getAllUsers, getUserById, updateById } from "../service/adminService";
import { NextFunction, Request, Response } from "express";
import { userDto } from "./dto/userDto";

async function getUsers(req: Request, res: Response) : Promise<void>{
    try{
        const users = await getAllUsers()
        res.json({users})
        return
    } catch(error) {
        res.json({message: error.message})
    }
}

async function getUser(req: Request, res: Response, next: NextFunction): Promise<void> {
   try{
        const id = req.params.id

        const user = await getUserById(id)

        if(!user)
            throw new Error("Incorrect id")

        res.json({user})
   } catch(error) {
    res.json({message : error.message})
   }
}

async function addUser(req: Request, res: Response, next: NextFunction): Promise<void>{
    try{
        const { username, password, role} = req.body

        await createNewUser(username, password, role)

        res.json({message: "User was created"})
    } catch(error) {
        res.json({message: error.message})
    }
}

async function deleteUser(req: Request, res: Response, next: NextFunction): Promise<void>{
    try{
        const id = req.params.id

        const user = await getUserById(id)

        if(!user)
            throw new Error("Incorrect id")

        await deleteById(id);
        res.json({message: "User " + user.username + "was deleted"})
    } catch(error) {
        res.json({message: error.message})
    }
}

async function updateUser(req: Request, res: Response, next: NextFunction): Promise<void>{
    try{
        const id = req.params.id

        const user = await getUserById(id)

        if(!user)
            throw new Error("Incorrect id")

        const updUser: userDto = req.body

        await updateById(id, updUser);
        res.json({message: "User was updated"})
    } catch(error) {
        res.json({message: error.message})
    }
}

export{
    getUsers,
    getUser,
    addUser,
    deleteUser,
    updateUser
}